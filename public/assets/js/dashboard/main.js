const EXISTE_TOKEN = localStorage.getItem("token");
if (EXISTE_TOKEN == null) {
    window.location.replace("/login");
}

let spanTotal = document.getElementsByClassName("tareas")[0];
let spanTotalCumplidos = document.getElementsByClassName("total-cumplidos")[0];
let ulTareas = document.getElementsByClassName("add-tareas")[0];
let formCrear = document.getElementById("crear-tarea");
let formEditar = document.getElementById("form-editar-tarea");
let btnCompletar = document.getElementsByClassName("completar-tarea");
let btnEliminar = document.getElementsByClassName("eliminar-tarea");
let btnEditar = document.getElementsByClassName("editar-tarea");

window.onload = function () {
    obtenerCantidadTareas();
    obtenerTareas();
};
//OBTENEMOS LOS DATOS DEL TOTAL TAREAS Y TOTAL TAREAS REALIZADAS
const obtenerCantidadTareas = async () => {
    fetch("api/consulta-total-tareas", {
        method: "GET", // or 'PUT'
        headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: EXISTE_TOKEN,
        },
    })
        .then((res) => res.json())
        .catch((error) => console.log("Error:", error))
        .then((response) => {
            const { total, totalCumplidas } = response;
            cantidadTareasRender(total, totalCumplidas);
        });
};

const cantidadTareasRender = async (total, totalCumplidas) => {
    spanTotal.innerHTML = total;
    spanTotalCumplidos.innerHTML = totalCumplidas;
};
//OBTENEMOS LAS TAREAS
const obtenerTareas = async () => {
    fetch("api/tareas", {
        method: "GET", // or 'PUT'
        headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: EXISTE_TOKEN,
        },
    })
        .then((res) => res.json())
        .catch((error) => console.log("Error:", error))
        .then((response) => {
            const { data } = response;
            renderTareas(data);
        });
};
const renderTareas = (tareas) => {
    let li = "";
    tareas.forEach((element) => {
        let checked = "";
        let classcheck = "tarea-pendiente";
        if (element.estado == "1") {
            checked = `checked=true`;
            classcheck = "tarea-terminada";
        }

        li += `
            <li  tarea-id="${element.id}">
                <label>
                    <input type="checkbox" ${checked} class="completar-tarea" tarea-id="${element.id}" estado="${element.estado}"><i class="check-box"></i>

                    <span>nombre: ${element.nombre} </span>
                    <span>descripción: ${element.descripcion}</span>
                    <span>fecha vencimiento : ${element.fecha_vencimiento}</span>
                    <a href='#' class="fa fa-times eliminar-tarea eliminar" tarea-id="${element.id}"></a>
                    <a href='#' class="fa fa-pencil editar-tarea editar" tarea-id="${element.id}" 
                    nombre="${element.nombre}"
                    desc="${element.descripcion}"
                    fecha="${element.fecha_vencimiento}"
                    data-toggle="modal" 
                    data-target="#modal-editar-tarea"></a>
                    <a href='#' class="fa fa-check ${classcheck}" ></a>
                </label>
            </li>
        `;
    });
    ulTareas.insertAdjacentHTML("beforeend", li);

    cambiarEstado();
    eliminarTarea();
    editarTarea();
};

formCrear.addEventListener("submit", function (e) {
    e.preventDefault();
    const formData = new FormData(document.getElementById("crear-tarea"));

    let data = {
        nombre: formData.get("nombre"),
        descripcion: formData.get("descripcion"),
        fecha_vencimiento: formData.get("fecha_vencimiento"),
    };
    fetch("api/tareas", {
        method: "POST", // or 'PUT'
        body: JSON.stringify(data), // data can be `string` or {object}!
        headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: EXISTE_TOKEN,
        },
    })
        .then((res) => res.json())
        .catch((error) => console.log("Error:", error))
        .then((response) => {
            const { mensaje, data } = response;
            Swal.fire({
                icon: "success",
                text: mensaje,
            });
            li = `
            <li tarea-id="${data.id}">
                    <label>
                        <input type="checkbox" tarea-id="${data.id}" estado="${data.estado}"><i class="check-box"></i>

                        <span>nombre: ${data.nombre} </span>
                        <span>descripción: ${data.descripcion}</span>
                        <span>fecha vencimiento : ${data.fecha_vencimiento}</span>
                        <a href='#' class="fa fa-times eliminar-tarea eliminar" tarea-id="${data.id}"></a>
                        <a href='#' class="fa fa-pencil editar-tarea editar" tarea-id="${data.id}" 
                        nombre="${data.nombre}"
                        desc="${data.descripcion}"
                        fecha="${data.fecha_vencimiento}"
                        data-toggle="modal" data-target="#modal-editar-tarea"></a>
                        <a href='#' class="fa fa-check" ></a>
                    </label>
                </li>
           `;

            ulTareas.insertAdjacentHTML("beforeend", li);
            cambiarEstado();
            eliminarTarea();
            editarTarea();
        });
});

const cambiarEstado = () => {
    Array.from(btnCompletar).forEach((element) => {
        element.addEventListener("click", function (e) {
            const tareaId = e.target.getAttribute("tarea-id");
            let estado = e.target.getAttribute("estado");

            if (estado == "1") {
                estado = "0";
            } else {
                estado = "1";
            }

            fetch(`api/cambiar-estado/${tareaId}/${estado}`, {
                method: "GET", // or 'PUT'
                headers: {
                    Accept: "application/json",
                    "Content-Type": "application/json",
                    Authorization: EXISTE_TOKEN,
                },
            })
                .then((res) => res.json())
                .catch((error) => console.log("Error:", error))
                .then((response) => {
                   
                    const { data, mensaje,code } = response;
                   
                    if(code == 200){
                        e.target.checked=false
                        Swal.fire({
                            icon: "success",
                            text: mensaje,
                        });
                        return
                    }
                    e.target.setAttribute("estado", data.estado);

                    if (
                        e.target.parentElement.children[7].classList.contains(
                            "tarea-pendiente"
                        )
                    ) {
                        e.target.parentElement.children[7].classList.remove(
                            "tarea-pendiente"
                        );
                        e.target.parentElement.children[7].classList.add(
                            "tarea-terminada"
                        );
                    } else {
                        e.target.parentElement.children[7].classList.remove(
                            "tarea-terminada"
                        );
                        e.target.parentElement.children[7].classList.add(
                            "tarea-pendiente"
                        );
                    }
                    Swal.fire({
                        icon: "success",
                        text: mensaje,
                    });
                    //   e.target.parentElement.children[7].classList.add(classcheck)
                    //   tarea-pendiente
                });
        });
    });
};

const eliminarTarea = () => {
    Array.from(btnEliminar).forEach((ele) => {
        ele.addEventListener("click", function (e) {
            let elemento = e.target.parentElement.parentElement;
            let tareaid = e.target.getAttribute("tarea-id");

            fetch(`api/tareas/${tareaid}`, {
                method: "DELETE", // or 'PUT'
                headers: {
                    Accept: "application/json",
                    "Content-Type": "application/json",
                    Authorization: EXISTE_TOKEN,
                },
            })
                .then((res) => res.json())
                .catch((error) => console.log("Error:", error))
                .then((response) => {
                    const { data, mensaje } = response;
                    elemento.remove();
                    Swal.fire({
                        icon: "success",
                        text: mensaje,
                    });
                });
        });
    });
};

const editarTarea = () => {
    Array.from(btnEditar).forEach((ele) => {
        ele.addEventListener("click", function (e) {
            const nombre = e.target.getAttribute("nombre");
            const desc = e.target.getAttribute("desc");
            const fecha = e.target.getAttribute("fecha");
            const tareaId = e.target.getAttribute("tarea-id");
            document.getElementById("editar-nombre").value = nombre;
            document.getElementById("editar-descrip").value = desc;
            document.getElementById("editar-fecha").value = fecha;
            document.getElementById("tareaId").value = tareaId;
        });
    });
};

formEditar.addEventListener("submit", function (e) {
    e.preventDefault();
    const formData = new FormData(document.getElementById("form-editar-tarea"));
    const tareaId = document.getElementById("tareaId").value;
    let data = {
        nombre: formData.get("nombre"),
        descripcion: formData.get("descripcion"),
        fecha_vencimiento: formData.get("fecha_vencimiento"),
    };

    fetch(`api/tareas/${tareaId}`, {
        method: "PUT", // or 'PUT'
        body: JSON.stringify(data), // data can be `string` or {object}!
        headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: EXISTE_TOKEN,
        },
    })
        .then((res) => res.json())
        .catch((error) => console.log("Error:", error))
        .then((response) => {
            const { mensaje } = response;
            Swal.fire({
                icon: "success",
                text: mensaje,
            });
            Array.from(ulTareas.children).forEach((ele) => {
                if (ele.getAttribute("tarea-id") == tareaId) {
                    ele.children[0].children[2].innerHTML = `nombre : ${data.nombre}`;
                    ele.children[0].children[3].innerHTML = `descripcion: ${data.descripcion}`;
                    ele.children[0].children[4].innerHTML = `fecha vencimiento: ${data.fecha_vencimiento}`;
                }
            });
        });
});
