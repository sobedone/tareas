let form = document.getElementById("iniciar-session");

form.addEventListener("submit", function (e) {
    e.preventDefault();

    const usuario_correo = document.getElementById("usuario_correo").value;
    const password = document.getElementById("password").value;

    let data = { usuario_correo, password };

    fetch("api/login", {
        method: "POST", // or 'PUT'
        body: JSON.stringify(data), // data can be `string` or {object}!
        headers: {
            "Content-Type": "application/json",
        },
    })
        .then((res) => res.json())
        .catch((error) => console.log("Error:", error))
        .then((response) => {
           
            const TOKEN_TYPE = response.token.token_type
            const TOKEN = response.token.access_token

            const TOKEN_COMPLETE = `${TOKEN_TYPE} ${TOKEN}`
         
            localStorage.setItem('token', TOKEN_COMPLETE);
          
            window.location.replace('/dashboard')
        });
});


