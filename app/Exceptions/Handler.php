<?php

namespace App\Exceptions;

use Throwable;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Validation\ValidationException;
use Illuminate\Foundation\Testing\HttpException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Exceptions\ThrottleRequestsException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

    public function render($request, Throwable $e)
    {
        $response = $this->handleException($request, $e);
        return $response;
    }

    private function handleException($request, Throwable $exception)
    {
        if ($exception instanceof ValidationException) {
            return $this->invalidJson($request, $exception);
        }

        if ($exception instanceof ModelNotFoundException) {
            $modelo = strtolower(class_basename($exception->getModel()));
            // return $this->errorResponse('No existe ninguna instancia de ' . $modelo . ' con el parámetro especificado.', 404);
            return response()->json(['mensaje'=>'No existe ninguna instancia de ' . $modelo . ' con el parámetro especificado.'], 404);
        }

        if ($exception instanceof AuthenticationException) {
            return $this->unauthenticated($request, $exception);
        }

        if ($exception instanceof AuthorizationException) {
            // return $this->errorResponse('No posee permisos para ejecutar esta acción.', 403);
            return response()->json(['mensaje'=>'No posee permisos para ejecutar esta acción.'], 403);
        }

        if ($exception instanceof NotFoundHttpException) {
            // return $this->errorResponse('No se encontró la URL especificada.', 404);
            // return response()->json(['mensaje'=>'No se encontró la URL especificada.'], 404);

            return redirect('/login');
        }
          // ========================================
        // Errores metodo no permitido
        // ========================================

        if ($exception instanceof MethodNotAllowedHttpException) {
            return response()->json(['mensaje'=>'El método especificado en la petición no es válido.'], 405);
            // return $this->errorResponse('El método especificado en la petición no es válido.', 405);
        }


        // ========================================
        // Limite de peticiones
        // ========================================

        if ($exception instanceof ThrottleRequestsException) {
            // return $this->errorResponse('Demasiados intentos.', 429);
            return response()->json(['mensaje'=>'Demasiados intentos'], 429);
        }

        // ========================================
        // Error HttpException
        // ========================================

        if ($exception instanceof HttpException) {
            return $this->errorResponse($exception->getMessage(), $exception->getStatusCode());
        }

        if (config('app.debug')) {
            return parent::render($request, $exception);
        }
    }
}
