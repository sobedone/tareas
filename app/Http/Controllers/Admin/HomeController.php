<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    public function __construct()
    {

    }

    public function inicio()
    {

        return view('auth.login');
    }

    public function dashboard(Request $request)
    {
        // if (is_null(auth()->user())) {
        //     return redirect('/login');
        // }
        
        
        return view('admin.tarea.index');
    }

    public function registro()
    {

        return view('auth.register');
    }
}
