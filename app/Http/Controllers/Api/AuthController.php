<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegistroRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    protected $usuarios;
    public function __construct(User $usuarios)
    {
        $this->usuarios = $usuarios;
    }

    public function registrarUsuario(RegistroRequest $request)
    {

      

        $usuarios = new $this->usuarios;
        $usuarios->fill([
            'usuario' => $request->get('usuario'),
            'correo' => $request->get('correo'),
            'nombre_completo' => $request->get('nombre_completo'),
            'password' => $request->get('password'),
        ]);

        if ($usuarios->save()) {
            return response()->json(['mensaje' => 'usuario creado'], 201);
        }

    }

    public function login(LoginRequest $request)
    {
        $login = $request->get('usuario_correo');
        $field = filter_var($login, FILTER_VALIDATE_EMAIL) ? 'correo' : 'usuario';

        $request->merge([
            $field => $login,
        ]);
        $credentials = $request->only($field, 'password');
        
        if (auth()->attempt($credentials)) {

            // if (auth()->user()->getRoleNames()->first() == 'administrador') {
            //     session()->put('selected_database', 'mysql_admin');
            // }
            $http = new \GuzzleHttp\Client;

            $uri = $_SERVER['SERVER_NAME'];
            $endPoint = $uri . '/oauth/token';

            $lastOauthClient = \DB::table('oauth_clients')->where('personal_access_client', '=', 0)->get()->last();
            $client = $lastOauthClient->secret;
            $idOauthClient = $lastOauthClient->id;

            $response = $http->post($endPoint, [
                'form_params' => [
                    'grant_type' => 'password',
                    'client_id' => $idOauthClient,
                    'client_secret' => $client,
                    'username' => auth()->user()->usuario,
                    'password' => $request->get('password'),
                    'scope' => '',
                ],
            ]);
            session(['usuario' => auth()->user()->id]);

            return ['token' => json_decode((string) $response->getBody(), true)];

        } else {

            return response()->json([
                'message' => 'Unauthorized',
            ], 401);

        }

    }

    /** ====================================================================

     * @author:roberto jose
     * @description:funcion que se encarga de cerrar la session del usuario y revocar el token generado en el login
     * @deprecated:false
     * @example:Ejemplo de la utilizacion del fragmento de codigo

     */

    public function logout(Request $request)
    {
       
        $request->user()->token()->revoke();
        return response()->json([
            'message' =>
            'Se desconectó con éxito'], 200);
    }

}
