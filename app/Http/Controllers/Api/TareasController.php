<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\TareaRequest;
use App\Models\Tareas;
use Illuminate\Http\Request;

class TareasController extends Controller
{

    protected $tarea;

    public function __construct(Tareas $tarea)
    {
        $this->tarea = $tarea;
    }

    // traer todas la data
    public function index()
    {
        
        if (auth()->user()->tipo == 1) {
            $tareas = $this->tarea->where('usuario_id', '=', auth()->user()->id)->orderBy('fecha_vencimiento', 'ASC')->get();
        } else {
            $tareas = $this->tarea->orderBy('fecha_vencimiento', 'ASC')->get();
        }
        return response()->json(['data' => $tareas], 202);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TareaRequest $request)
    {
        $tarea = new $this->tarea;

        $tarea->fill([
            'nombre' => $request->get('nombre'),
            'descripcion' => $request->get('descripcion'),
            'fecha_vencimiento' => $request->get('fecha_vencimiento'),
            'usuario_id' => auth()->user()->id,
        ]);

        if ($tarea->save()) {
            return response()->json(['data' => $tarea, 'mensaje' => 'tarea creada'], 201);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $tieneLatarea = $this->tarea->verificarEdicion($id);

        if ($tieneLatarea) {
            return response()->json(['data' => [], 'mensaje' => 'no puedes actualizar una tarea que no es tuya'], 200);
        } else {
            $tareas = $this->tarea->findOrFail($id);
            return response()->json(['data' => $tareas], 202);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $tieneLatarea = $this->tarea->verificarEdicion($id);
        if ($tieneLatarea) {
            return response()->json(['data' => [], 'mensaje' => 'no puedes modificar una tarea que no es tuya'], 200);
        } else {
            $tareas = $this->tarea->findOrFail($id);
            $tareas->fill([
                'nombre' => $request->get('nombre'),
                'descripcion' => $request->get('descripcion'),
                'fecha_vencimiento' => $request->get('fecha_vencimiento'),
                // 'estado'=>$request->get('estado'),
                // 'usuario_id' => auth()->user()->id,
            ]);

            if ($tareas->save()) {
                return response()->json(['mensaje' => 'Registro modificado'], 202);
            }

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tieneLatarea = $this->tarea->verificarEdicion($id);
        if ($tieneLatarea) {
            return response()->json(['data' => [], 'mensaje' => 'no puedes eliminar una tarea que no es tuya'], 200);
        } else {

            $tareas = $this->tarea->findOrFail($id);
            if ($tareas->delete()) {
                return response()->json(['data' => $tareas, 'mensaje' => 'Registro eliminado'], 200);
            }
        }
    }

    public function totalTareas()
    {

        if (auth()->user()->tipo == 1) {
            $total = $this->tarea->where('usuario_id', '=', auth()->user()->id)->count();
            $totalCumplidas = $this->tarea->where([['estado', '=', '1'],['usuario_id', '=', auth()->user()->id]])->count();

        }else{
            $total = $this->tarea->count();
            $totalCumplidas = $this->tarea->where([['estado', '=', '1']])->count();
        }

        return response()->json(['total' => $total, 'totalCumplidas' => $totalCumplidas], 202);
    }

    public function cambiarEstado($id, $estado)
    {
        $tieneLatarea = $this->tarea->verificarEdicion($id);
        if ($tieneLatarea) {
            return response()->json(['data' => [], 'mensaje' => 'no puedes modificar una tarea que no es tuya','code'=>200], 200);
        } else {

            $tareas = $this->tarea->findOrFail($id);
            $tareas->fill([
                'estado' => $estado,
            ]);

            $tareas->save();
            if ($tareas->estado == 1) {
                $mensaje = 'Tarea terminada';
            } else {
                $mensaje = 'Tarea pendiente';
            }

            return response()->json(['data' => $tareas, 'mensaje' => $mensaje], 202);
        }
    }
}
