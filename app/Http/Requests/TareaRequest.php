<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TareaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */


    public function rules()
    {
       

        return [
            'nombre' => 'required',
            'fecha_vencimiento' => 'required',
            
        ];
    }

    public function messages()
    {
        return [
            'nombre.required' => 'El :attributes es obligatorio.',
            'fecha_vencimiento.required' => 'La :attributes es obligatorio',
           
            
            
        ];
    }
    public function attributes()
    {
        return [
            'nombre' => 'Nombre de la tarea',
            'fecha_vencimiento' => 'Fecha de vencimiento',
          
        ];
    }
}
