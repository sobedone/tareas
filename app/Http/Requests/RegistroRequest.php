<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegistroRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'usuario' => 'required',
            'correo' => 'required',            
            'nombre_completo' => 'required',
            'password' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'usuario.required' => 'La :attributes es obligatorio.',
            'correo.required' => 'El :attributes es obligatorio',
            'nombre_completo.required' => 'El :attributes es obligatorio',
            'password.required' => 'La :attributes es obligatorio',
            
        ];
    }
    public function attributes()
    {
        return [
            'usuario' => 'Nombre usuario',
            'correo' => 'Correo electronico',
            'nombre_completo' => 'nombre completo',
            'password' => 'Contraseña',
        ];
    }
}
