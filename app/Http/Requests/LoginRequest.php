<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'usuario_correo' => 'required',
            'password' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'usuario_correo.required' => 'La :attributes es obligatorio.',
            'password.required' => 'La :attributes es obligatorio',
            
            
        ];
    }
    public function attributes()
    {
        return [
            'usuario_correo' => 'Nombre usuario o correo',
            'password' => 'Contraseña',
        ];
    }
}
