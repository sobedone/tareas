<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tareas extends Model
{
    use HasFactory;

    
    protected $table = 'tareas';
    protected $fillable = [
        'nombre',
        'descripcion',
        'estado',
        'fecha_vencimiento',
        'usuario_id'
    ];

    public function usuarios()
    {
        return $this->belongsTo('App\Models\User','usuario_id');
    }

    public function verificarEdicion($id)
    {
        $return = $this->where([['id','=',$id],['usuario_id','=',auth()->user()->id]])->first();

        if (is_null($return)) {
            return true;
        }else{
            return false;
        }
       
    }

}
