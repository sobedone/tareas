@extends('admin.template.base')

@push('css')
    <link rel="stylesheet" href="./assets/css/tareas/main.css">
@endpush
@section('contenido')
    <!-- Animated -->
    <div class="animated fadeIn">
        <!-- Widgets  -->
        <div class="row">
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div class="stat-widget-five">
                            {{-- <div class="stat-icon dib flat-color-1">
                                <i class="pe-7s-cash"></i>
                            </div> --}}
                            <div class="stat-content">
                                <div class="text-left dib">
                                    <div class="stat-text"><span class="tareas">0</span></div>
                                    <div class="stat-heading">tareas</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div class="stat-widget-five">
                            {{-- <div class="stat-icon dib flat-color-2">
                                <i class="pe-7s-cart"></i>
                            </div> --}}
                            <div class="stat-content">
                                <div class="text-left dib">
                                    <div class="stat-text"><span class="total-cumplidos">0</span></div>
                                    <div class="stat-heading">tareas realizadas</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- /Widgets -->
    
        <div class="clearfix"></div>
        <!-- Orders -->
    
        <!-- /.orders -->
        <!-- To Do and Live Chat -->
        <div class="row">
            <div class="col-lg-12 crear-tarea">
                <button type="button" class="btn btn-primary agregar-tarea"  data-toggle="modal" data-target="#modal-crear-tarea">Crear Tarea</button>
            </div>
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title box-title">To Do List</h4>
                        <div class="card-content">
                            <div class="todo-list">
                                <div class="tdl-holder">
                                    <div class="tdl-content">
                                        <ul class="add-tareas">
                                            
                                          
                                        </ul>
                                    </div>
                                </div>
                            </div> <!-- /.todo-list -->
                        </div>
                    </div> <!-- /.card-body -->
                </div><!-- /.card -->
            </div>
        </div>
        <!-- /To Do and Live Chat -->
      
        <!-- Modal - Calendar - Add New Event -->
        <div class="modal fade none-border" id="modal-crear-tarea">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title"><strong>Nueva tarea</strong></h4>
                    </div>
                    <div class="modal-body">

                        <div class="card">
                            <div class="card-body card-block">
                                <form id="crear-tarea">
                                    
                                    <div class="form-group">
                                       <label for="nombre" class=" form-control-label">Nombre tarea</label>
                                       <input type="text" id="nombre" name="nombre" placeholder="Nombre tarea" class="form-control" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="descripcion" class=" form-control-label">descripcion</label>
                                        <textarea type="text" name="descripcion" id="descripcion" rows="4" placeholder="Content..." class="form-control"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="fecha_vencimiento" class=" form-control-label">Fecha vencimiento</label>
                                        <input type="date" id="fecha_vencimiento" name="fecha_vencimiento" placeholder="2020-01-28" class="form-control" required>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">cancelar</button>
                                        <button type="submit" class="btn btn-primary waves-effect">crear evento</button>
                                    </div>
                                </form>
                            </div>
                            
                        </div>
                    </div>
                   
                </div>
            </div>
        </div>

        <div class="modal fade none-border" id="modal-editar-tarea">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title"><strong>Editar tarea</strong></h4>
                    </div>
                    <div class="modal-body">

                        <div class="card">
                            <div class="card-body card-block">
                                <form id="form-editar-tarea">
                                    <input type="hidden" id="tareaId">
                                    
                                    <div class="form-group">
                                       <label for="nombre" class=" form-control-label">Nombre tarea</label>
                                       <input type="text"  name="nombre" id="editar-nombre" placeholder="Nombre tarea" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="descripcion" class=" form-control-label">descripcion</label>
                                        <textarea type="text" name="descripcion" id="editar-descrip" rows="4" placeholder="Content..." class="form-control"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="fecha_vencimiento" class=" form-control-label">Fecha vencimiento</label>
                                        <input type="date" name="fecha_vencimiento" id="editar-fecha" placeholder="2020-01-28" class="form-control">
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">cancelar</button>
                                        <button type="submit" class="btn btn-primary waves-effect">Actualizar evento</button>
                                    </div>
                                </form>
                            </div>
                            
                        </div>
                    </div>
                   
                </div>
            </div>
        </div>


        <!-- /#event-modal -->

    </div>
    <!-- .animated -->
@endsection
    
@push('scripts')
    <script src="{{ asset('/assets/js/dashboard/main.js') }}"></script>
@endpush