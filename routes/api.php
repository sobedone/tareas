<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::middleware('auth:api')->group(function () {
    Route::get('/logout', ['as' => 'logout_usurio', 'uses' => 'Api\AuthController@logout']);
    Route::apiResource('/tareas', 'Api\TareasController');
    Route::get('/consulta-total-tareas', ['as' => 'total_tareas', 'uses' => 'Api\TareasController@totalTareas']);
    Route::get('/cambiar-estado/{tareaId}/{estado}', ['as' => 'total_tareas', 'uses' => 'Api\TareasController@cambiarEstado']);
});

//ruta para el registro del usuario

Route::post('/registro', ['as' => 'registroUsuario', 'uses' => 'Api\AuthController@registrarUsuario']);
Route::post('/login', ['as' => 'login_usuario', 'uses' => 'Api\AuthController@login']);
