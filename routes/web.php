<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/login', ['as' => 'login_usuario', 'uses' => 'Admin\HomeController@inicio']);
Route::get('/dashboard', ['as' => 'login_usuario', 'uses' => 'Admin\HomeController@dashboard']);
Route::get('/registro', ['as' => 'login_usuario', 'uses' => 'Admin\HomeController@registro']);
