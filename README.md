## documentacion api
https://documenter.getpostman.com/view/408801/TzRRDoqy

## instalacion de proyecto

1) composer install
2) crear la  base de datos el nombre se encuentra en .env.example
3) corre comando php artisan migration
4) php artisan passport:install
5) php artisan db:seed
6) usuarios con estado 1 son usarios normales, con estado 2 son administradores pueden ver todo
7) contraseña 12345 para cualquier usuario

## repositorio proyecto
https://bitbucket.org/sobedone/tareas/commits/
## clone de proyecto
git clone https://sobedone@bitbucket.org/sobedone/tareas.git


