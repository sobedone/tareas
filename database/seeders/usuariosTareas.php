<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Modelos\Tareas;
use App\Modelos\User;
class usuariosTareas extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        \DB::table('usuarios')->truncate();
        \DB::table('tareas')->truncate();
   
        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $usuarios = factory(User::class, 3)->create();

    }
}
