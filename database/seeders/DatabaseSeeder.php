<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Database\Seeders\usuariosTareas;
use App\Models\User;
use App\Models\Tareas;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
       
    //    $this->call(usuariosTareas::class);
        User::factory(3)->create();
        Tareas::factory(15)->create();
    }
}
