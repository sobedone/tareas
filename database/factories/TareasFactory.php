<?php

namespace Database\Factories;

use App\Models\Tareas;
use Illuminate\Database\Eloquent\Factories\Factory;

use App\Models\User;
class TareasFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Tareas::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
      
        return [
            'nombre'=>$this->faker->name(),
            'descripcion'=>$this->faker->name(),
            'estado'=>$this->faker->randomElement(['0' ,'1']),
            'fecha_vencimiento' =>$this->faker->date('Y-m-d','now'),
            'usuario_id'=>function () {
                return User::factory()->create()->id;
            },
           
        ];
    }
}
